import { Controller, Get, Param, Post, Put, Body } from '@nestjs/common';
import { FaqCategoryService } from './faqCategory.service';
import { FaqCategoryDto } from '../dto/faqCategory.dto';
import { DataWrapper } from '../response/dataWrapper';

@Controller('faq-category')
export class FaqCategoryController {
  constructor(
    private readonly FaqCategoryService: FaqCategoryService,
    private readonly DataWrapper: DataWrapper,
  ) {}

  @Get('find-all')
  async findAll() {
    const data = await this.FaqCategoryService.findAll();
    return this.DataWrapper.find(data);
  }

  @Post('create')
  async create(@Body() FaqCategory: FaqCategoryDto) {
    await this.FaqCategoryService.create(FaqCategory);
    return this.DataWrapper.created(FaqCategory);
  }

  @Get('find-one/:id')
  async findOne(@Param() params) {
    const data = await this.FaqCategoryService.findOne(params.id);
    return this.DataWrapper.find(data);
  }

  @Put('update/:id')
  async update(@Param('id') id, @Body() Faq: FaqCategoryDto): Promise<any> {
    Faq.id = Number(id);
    this.FaqCategoryService.update(Faq);
    return this.DataWrapper.find(Faq);
  }

  @Put('delete/:id')
  async remove(@Param('id') id, @Body() Faq: FaqCategoryDto): Promise<any> {
    Faq.id = Number(id);
    this.FaqCategoryService.remove(Faq);
    return this.DataWrapper.deleted();
  }
}
