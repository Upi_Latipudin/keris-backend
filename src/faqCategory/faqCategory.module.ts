import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { FaqCategoryEntity } from '../entity/faqCategory.entity';
import { FaqCategoryController } from './faqCategory.controller';
import { FaqCategoryService } from './faqCategory.service';
import { DataWrapper } from '../response/dataWrapper';

@Module({
  imports: [TypeOrmModule.forFeature([FaqCategoryEntity])],
  exports: [TypeOrmModule],
  controllers: [FaqCategoryController],
  providers: [FaqCategoryService, DataWrapper],
})
export class FaqCategoryModule {}
