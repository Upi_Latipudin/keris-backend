import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, UpdateResult } from 'typeorm';
import { FaqCategoryEntity } from '../entity/faqCategory.entity';

@Injectable()
export class FaqCategoryService {
  constructor(
    @InjectRepository(FaqCategoryEntity)
    private faqRepository: Repository<FaqCategoryEntity>,
  ) {}

  async findAll(): Promise<FaqCategoryEntity[]> {
    return this.faqRepository.find({
      where: {
        is_deleted: false,
      },
    });
  }

  async create(
    FaqCategoryEntity: FaqCategoryEntity,
  ): Promise<FaqCategoryEntity> {
    FaqCategoryEntity.created_at = new Date();
    FaqCategoryEntity.created_by = 1;
    return await this.faqRepository.save(FaqCategoryEntity);
  }

  async findOne(id: number): Promise<FaqCategoryEntity> {
    return this.faqRepository.findOne({
      where: {
        id: id,
        is_deleted: false,
      },
    });
  }

  async update(FaqCategoryEntity: FaqCategoryEntity): Promise<UpdateResult> {
    FaqCategoryEntity.updated_at = new Date();
    FaqCategoryEntity.updated_by = 1;
    return await this.faqRepository.update(
      FaqCategoryEntity.id,
      FaqCategoryEntity,
    );
  }

  async remove(FaqCategoryEntity: FaqCategoryEntity): Promise<UpdateResult> {
    FaqCategoryEntity.deleted_at = new Date();
    FaqCategoryEntity.deleted_by = 1;
    FaqCategoryEntity.is_deleted = true;
    return await this.faqRepository.update(
      FaqCategoryEntity.id,
      FaqCategoryEntity,
    );
  }
}
