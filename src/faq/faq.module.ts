import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { FaqEntity } from '../entity/faq.entity';
import { FaqController } from './faq.controller';
import { FaqService } from './faq.service';
import { DataWrapper } from '../response/dataWrapper';

@Module({
  imports: [TypeOrmModule.forFeature([FaqEntity])],
  exports: [TypeOrmModule],
  controllers: [FaqController],
  providers: [FaqService, DataWrapper],
})
export class FaqModule {}
