import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, UpdateResult } from 'typeorm';
import { FaqEntity } from '../entity/faq.entity';

@Injectable()
export class FaqService {
  constructor(
    @InjectRepository(FaqEntity)
    private faqRepository: Repository<FaqEntity>,
  ) {}

  async findAll(): Promise<FaqEntity[]> {
    return this.faqRepository.find({
      where: {
        is_deleted: false,
      },
    });
  }

  async create(FaqEntity: FaqEntity): Promise<FaqEntity> {
    FaqEntity.created_at = new Date();
    FaqEntity.created_by = 1;
    return await this.faqRepository.save(FaqEntity);
  }

  async findOne(id: number): Promise<FaqEntity> {
    return this.faqRepository.findOne({
      where: {
        id: id,
        is_deleted: false,
      },
    });
  }

  async update(FaqEntity: FaqEntity): Promise<UpdateResult> {
    FaqEntity.updated_at = new Date();
    FaqEntity.updated_by = 1;
    return await this.faqRepository.update(FaqEntity.id, FaqEntity);
  }

  async remove(FaqEntity: FaqEntity): Promise<UpdateResult> {
    FaqEntity.deleted_at = new Date();
    FaqEntity.deleted_by = 1;
    FaqEntity.is_deleted = true;
    return await this.faqRepository.update(FaqEntity.id, FaqEntity);
  }
}
