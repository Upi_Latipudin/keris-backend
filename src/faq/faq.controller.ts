import { Controller, Get, Param, Post, Put, Body } from '@nestjs/common';
import { FaqService } from './faq.service';
import { FaqDto } from '../dto/faq.dto';
import { DataWrapper } from '../response/dataWrapper';

@Controller('faq')
export class FaqController {
  constructor(
    private readonly FaqService: FaqService,
    private readonly DataWrapper: DataWrapper,
  ) {}

  @Get('find-all')
  async findAll() {
    const data = await this.FaqService.findAll();
    return this.DataWrapper.find(data);
  }

  @Post('create')
  async create(@Body() Faq: FaqDto) {
    await this.FaqService.create(Faq);
    return this.DataWrapper.created(Faq);
  }

  @Get('find-one/:id')
  async findOne(@Param() params) {
    const data = await this.FaqService.findOne(params.id);
    return this.DataWrapper.find(data);
  }

  @Put('update/:id')
  async update(@Param('id') id, @Body() Faq: FaqDto): Promise<any> {
    Faq.id = Number(id);
    this.FaqService.update(Faq);
    return this.DataWrapper.find(Faq);
  }

  @Put('delete/:id')
  async remove(@Param('id') id, @Body() Faq: FaqDto): Promise<any> {
    Faq.id = Number(id);
    this.FaqService.remove(Faq);
    return this.DataWrapper.deleted();
  }
}
