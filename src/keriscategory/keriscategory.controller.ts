import { Controller, Get, Param, Post,Body, Res, HttpStatus,HttpService  } from '@nestjs/common';
import { KerisCategoryService } from './keriscategory.service';
import { KerisCategory } from '../entity/keris.entity';
import { KerisCategoryDataWrapper } from './response/keriscategoryDataWrapper';


@Controller('keris-category')
export class KerisCategoryController {
  constructor(private readonly kerisService: KerisCategoryService,
		  private readonly kerisCategoryDataWrapper : KerisCategoryDataWrapper) {}
  
  @Get('findAll')
  async findAll() {
    const keris =  await this.kerisService.findAll();
    return this.kerisCategoryDataWrapper.findAll(keris);
  }
  
	@Post('create')
	async create(@Body() kerisDto: KerisCategory) {
	   await this.kerisService.create(kerisDto);
	   return this.kerisCategoryDataWrapper.complateCreate(kerisDto);
	}
  
	
	@Get('findOne/:id')
	  async findOne(@Param() params) {
		const keris = await this.kerisService.findOne(params.id);
	    return this.kerisCategoryDataWrapper.find(keris,params.id);
	}
	
	  @Get('delete/:id')
	  async remove(@Param() params): Promise<void> {
	    this.kerisService.remove(params.id);
	}
}
