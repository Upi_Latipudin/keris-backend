import { HttpException, HttpStatus } from "@nestjs/common";

HttpStatus
export class KerisCategoryDataWrapper {

	
	public complateCreate(data: any){
		return {
			statusCode: HttpStatus.CREATED,
			message :" Keris Category has successfully created!",
			data : data
		}
	}
	
	public find(data: any, id:number){
		if(data){
			return {
				statusCode: HttpStatus.OK,
				data : data,
				message :`Keris with id ${id} found`,
			}
		} else {
			throw new HttpException(`Keris with id ${id} not found`, HttpStatus.NOT_FOUND);
		}
		
	}
	
	public delete(){
			return {
				statusCode: HttpStatus.NO_CONTENT,
				message :`Keris Category with id  deleted!`,
			}
	}
	
	public findAll(data: any){
		if(data.length!=0){
			return {
				statusCode: HttpStatus.OK,
				data : data
			} 
		} else {
				throw new HttpException(`Keris category not found data`, HttpStatus.OK);
		}
	}
	
	
}