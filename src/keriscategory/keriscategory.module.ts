import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { KerisCategory } from '../entity/keris.entity';
import { KerisCategoryController } from './keriscategory.controller';
import { KerisCategoryService } from './keriscategory.service';
import { KerisCategoryDataWrapper } from './response/keriscategoryDataWrapper';

@Module({
  imports: [TypeOrmModule.forFeature([KerisCategory])],
  exports: [TypeOrmModule],
  controllers: [KerisCategoryController],
  providers: [KerisCategoryService,KerisCategoryDataWrapper]
})
export class KerisModule {}