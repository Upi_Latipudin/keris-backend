import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { KerisCategory } from '../entity/keris.entity';
import { KerisDto } from '../dto/keris.dto';

@Injectable()
export class KerisCategoryService {
  constructor(
    @InjectRepository(KerisCategory)
    private usersRepository: Repository<KerisCategory>,
  ) {}

  findAll(): Promise<KerisCategory[]> {
    return this.usersRepository.find();
  }

  findOne(id: number): Promise<KerisCategory> {
    return this.usersRepository.findOne(id);
  }

  async remove(id: string): Promise<void> {
    await this.usersRepository.delete(id);
  }
  
  async create(kerisDto: KerisCategory): Promise<KerisCategory> {
	   
	    return (await this.usersRepository.save(kerisDto));
	  }
}