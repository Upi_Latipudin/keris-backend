export interface Keris {
  name: string;
  age: number;
  breed: string;
}