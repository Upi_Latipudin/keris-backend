import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
//import { KerisCategoryService } from './keriscategory.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { KerisCategory } from './entity/keris.entity';
import { KerisModule } from './keriscategory/keriscategory.module';
import { User } from './entity/user.entity';
import { UserModule } from './users/users.module';
import { FaqModule } from './faq/faq.module';
import { FaqCategoryModule } from './faqCategory/faqCategory.module';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: '127.0.0.1',
      port: 3306,
      username: 'root',
      password: '',
      database: 'keris',
      entities: [__dirname + '/*/.entity{.ts,.js}'],
      synchronize: true,
      autoLoadEntities: true,
    }),
    KerisModule,
    UserModule,
    FaqModule,
    FaqCategoryModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
