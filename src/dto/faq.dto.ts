export class FaqDto {
  id: number;
  topic: string;
  description: string;
  is_publish: boolean;
  id_faq_category: number;
  created_at: Date;
  created_by: number;
  updated_at: Date;
  updated_by: number;
  deleted_at: Date;
  deleted_by: number;
  is_deleted: boolean;
}
