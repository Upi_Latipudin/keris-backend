export class KerisDto {
  readonly name: string;
  readonly age: number;
  readonly breed: string;
}