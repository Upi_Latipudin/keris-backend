export class UserDTO {
  
	id: number;

	firstName: string;

	lastName: string;

	userName: string;

	password: string;

	confirmationPassword : string;

	email: string;
  
	noTlp: string;
  
	brithDate: Date;
  
	address: string;

	activated : boolean;
}