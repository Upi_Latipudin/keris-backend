export class FaqCategoryDto {
  id: number;
  name: string;
  created_at: Date;
  created_by: number;
  updated_at: Date;
  updated_by: number;
  deleted_at: Date;
  deleted_by: number;
  is_deleted: boolean;
}
