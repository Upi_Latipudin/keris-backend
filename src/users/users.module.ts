import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from '../entity/user.entity';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';
import { UserDataWrapper } from './response/user-data-wrapper';

@Module({
  imports: [TypeOrmModule.forFeature([User])],
  exports: [TypeOrmModule],
  controllers: [UsersController],
  providers: [UsersService,UserDataWrapper]
})
export class UserModule {}