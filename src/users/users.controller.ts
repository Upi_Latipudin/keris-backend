import { Controller, Get, Param, Post,Body  } from '@nestjs/common';
import { UsersService } from './users.service';
import { User } from '../entity/user.entity';
import { UserDataWrapper } from './response/user-data-wrapper';
import { UserDTO } from 'src/dto/user-dto';


@Controller('user')
export class UsersController {
  constructor(private readonly usersService: UsersService,
		  private readonly userDataWrapper : UserDataWrapper) {}
  
  @Get('findAll')
  async findAll() {
    const user =  await this.usersService.findAll();
    return this.userDataWrapper.findAll(user);
  }
  
	@Post('create')
	async create(@Body() user: UserDTO){
		const usr = await	this.usersService.create(user);
			return this.userDataWrapper.complateCreate(usr);
		
	}
  
	
	@Get('findOne/:id')
	  async findOne(@Param() params) {
		const user = await this.usersService.findOne(params.id);
	    return this.userDataWrapper.find(user, params.id);
	}
	
	  @Get('delete/:id')
	  async remove(@Param() params): Promise<void> {
	    return this.usersService.remove(params.id);
	}
}
