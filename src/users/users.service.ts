import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from '../entity/user.entity';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'src/constant/input.constant';
import { UserDTO } from 'src/dto/user-dto';
import * as bcrypt from 'bcrypt';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private usersRepository: Repository<User>,
  ) {}

  findAll(): Promise<User[]> {
    return this.usersRepository.find();
  }

  findOne(id: number): Promise<User> {
    return this.usersRepository.findOne(id);
  }

  async remove(id: string): Promise<void> {
    await this.usersRepository.delete(id);
  }
  
 
  async create(user: UserDTO): Promise<User> {
	  this.convertDateFromClient(user);
	  const usr = await this.passwordHasing(user);
	    return await this.usersRepository.save(usr);
	  }
  
  protected convertDateFromClient(res: User): User {
	    if (res) {
	    	 moment(res.brithDate).format(DATE_TIME_FORMAT); 
	    }
	    return res;
	  }
  
  public async passwordHasing(user:UserDTO) : Promise<any>{
	  const saltOrRounds = 10;
	  user.password = await bcrypt.hash(user.password, saltOrRounds);
	  user.confirmationPassword = await bcrypt.hash(user.confirmationPassword, saltOrRounds);
	  return user;
  }
  
}