import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class KerisCategory {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  age: number;

  @Column({ default: true })
  breed: string;
}
