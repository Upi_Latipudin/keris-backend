import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity({ name: 'mst_faq' })
export class FaqEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  topic: string;

  @Column()
  description: string;

  @Column({ default: false })
  is_publish: boolean;

  @Column()
  id_faq_category: number;

  @Column({ type: 'datetime', default: null })
  created_at: Date;

  @Column({ default: null })
  created_by: number;

  @Column({ type: 'datetime', default: null })
  updated_at: Date;

  @Column({ default: null })
  updated_by: number;

  @Column({ type: 'datetime', default: null })
  deleted_at: Date;

  @Column({ default: null })
  deleted_by: number;

  @Column({ default: false })
  is_deleted: boolean;
}
