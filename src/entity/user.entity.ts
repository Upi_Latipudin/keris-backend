import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  firstName: string;

  @Column()
  lastName: string;

  @Column()
  userName: string;

  @Column()
  password: string;

  @Column()
  confirmationPassword : string;

  @Column()
  email: string;
  
  @Column()
  noTlp: string;
  
  @Column()
  brithDate: Date;
  
  @Column()
  address: string;
  
  @Column()
  activated : boolean;
}