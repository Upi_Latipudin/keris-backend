import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity({ name: 'mst_faq_category' })
export class FaqCategoryEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column({ type: 'datetime', default: null })
  created_at: Date;

  @Column({ default: null })
  created_by: number;

  @Column({ type: 'datetime', default: null })
  updated_at: Date;

  @Column({ default: null })
  updated_by: number;

  @Column({ type: 'datetime', default: null })
  deleted_at: Date;

  @Column({ default: null })
  deleted_by: number;

  @Column({ default: false })
  is_deleted: boolean;
}
