import { Controller, Get, Param, Post,Body, Res  } from '@nestjs/common';
import { AppService } from './app.service';


@Controller('keris-category')
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }
  
  @Get('edit/:id')
  edit(@Param() params): string {
    console.log(params.id);
    return `ini edit dengan id #${params.id}`;
  }
  
  @Post('edit/:id')
  update(@Param() params): string {
    console.log(params.id);
    return `ini update dengan id #${params.id}`;
  }
  

}
