import { HttpException, HttpStatus } from '@nestjs/common';

HttpStatus;
export class DataWrapper {
  public created(data: any) {
    return {
      statusCode: HttpStatus.CREATED,
      message: 'Data has successfully created.',
      data: data,
    };
  }

  public updated(data: any) {
    return {
      statusCode: HttpStatus.OK,
      message: 'Data has successfully udpated.',
      data: data,
    };
  }

  public find(data: any) {
    if (data) {
      return {
        statusCode: HttpStatus.OK,
        data: data,
      };
    } else {
      throw new HttpException('Data not found.', HttpStatus.NOT_FOUND);
    }
  }

  public deleted() {
    return {
      statusCode: HttpStatus.OK,
      message: 'Data has successfully deleted.',
    };
  }
}
